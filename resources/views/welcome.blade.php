@extends('layouts.app')

@section('content')
<div class="container">
        <div class="flex-center position-ref full-height">
            <div id="form-messages"></div>
            <form>
                {{csrf_field()}}
                <div class="form-group">
                    <label>Product name</label>
                    <input type="text" class="form-control" name="product">
                </div>
                <div class="form-group">
                    <label>Quantity in stock</label>
                    <input type="number" class="form-control" name="quantity">
                </div>
                <div class="form-group">
                    <label>Price per item</label>
                    <input type="number" class="form-control" name="price">
                </div>
                <button class="btn btn-success btn-submit">Submit</button>
            </form>
        </div>       
</div>
<br>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Product name</th>
                <th scope="col">Quantity in stock</th>
                <th scope="col">Price per item</th>
                <th scope="col">Datetime submitted</th>
                <th scope="col">Total value number</th>
                <th scope="col">Operations</th>
            </tr>
        </thead>
        <tbody>
            @if(count($json) > 0)
                @php
                    $sum =0
                @endphp
                @foreach($json as $json_item)
                    <tr>
                        <td>{{$json_item->product}}</td>
                        <td class="text-center">{{$json_item->quantity}}</td>
                        <td class="text-right">{{number_format((float)$json_item->price, 2, '.', '')}}</td>
                        <td>{{$json_item->datetime_submitted}}</td>
                        <td  class="text-right">{{number_format((float)$json_item->price * $json_item->quantity, 2, '.', '')}}
                            @php
                                $sum += $json_item->price * $json_item->quantity
                            @endphp
                        </td>
                        <td>
                            <button type="button" class="btn btn-primary btn-sm">Edit</button> 
                            <button type="button" class="btn btn-danger btn-sm">Delete</button>
                        </td>
                    </tr>
                @endforeach 
                <tr>
                    <td colspan="3"></td>
                    <td class="text-right">Total:</td>
                    <td class="text-right">{{number_format((float)$sum, 2, '.', '')}}</td>
                    <td></td>
                </tr>
            @endif
        </tbody> 
    </table>

</div>

@endsection

@section('ajaxpost')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(".btn-submit").click(function(e){
        e.preventDefault();
        var product = $("input[name=product]").val();
        var quantity = $("input[name=quantity]").val();
        var price = $("input[name=price]").val();

        $.ajax({
           type:'POST',
           url:'/ajaxRequest',
           data:{product:product, quantity:quantity, price:price},
           success:function(data){
              alert(data.success); 
              location.reload();    
           }
        });
	});
</script>
@endsection
