<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    public function index()
    {
        $json = json_decode(Storage::disk('public')->get('data.json'));
        return view('welcome')->with('json', $json);
    }

    public function ajaxRequest()
    {
        $json = json_decode(file_get_contents("data.json"), true); 
        return view('welcome')->with('json', $json);

    }

    public function ajaxRequestPost(Request $request){

        $fileCheck = Storage::disk('public')->exists('data.json') ? json_decode(Storage::disk('public')->get('data.json')) : [];
        $requestData = $request->all();
        $requestData['datetime_submitted'] = date('Y-m-d H:i:s');

        array_push($fileCheck,$requestData);
        Storage::disk('public')->put('data.json', json_encode($fileCheck));
        $message = 'Data inserted';
        return response()->json(['success'=> $message]);
    }

}
