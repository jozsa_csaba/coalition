<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', 'ProductsController@index');
Route::get('/', 'ProductsController@index');

Route::get('ajaxRequest', 'ProductsController@ajaxRequest');
Route::post('ajaxRequest', 'ProductsController@ajaxRequestPost');